var requireEnv = require("require-environment-variables");
if(process.env.NODE_ENV === 'production') {
  try {
    requireEnv(['COOKIE_SECRET', 'JWT_SECRET']);
  } catch (e) {
    process.exit(1);
  }
}

var _ = require('lodash');
var db = require('./db');
var requireDir = require('../utils/requireDir');
var path = require('path');
var Promise = require('bluebird');

var logger = require('./logger');

var API = {
  _: _,
  Promise: Promise,
  logger: logger
};

module.exports.init = function () {
  return new Promise(function(resolve, reject) {
    new db.init({
      dbPath:  process.env.DB_PATH || __dirname + '/../../db.sqlite',
      modelsPath: __dirname + '/../../api/models'
    }).then(function(models){
      API.models = models;

      var services = requireDir(__dirname + '/../../api/services');
      API.services = {};
       _.each(services, function (service, key) {
         API.services[key] = {};
         if (_.isFunction(service)) {
           API.services[key] = service.bind(_.assign({ API:API }, this));
         } else {
           _.each(service, function (v, k) {
             if (_.isFunction(v)) {
               API.services[key][k] = v.bind(_.assign({ API:API }, this));
             } else {
               API.services[key][k] = v;
             }
           });
         }
      });

      var servicesInitPromises = [];

      var filtered = _.sortBy(
        _.filter(API.services, function(o) {
          return o.init && !o.requiresApp;
        }),  function(o){
          return o.loadOrder;
        });

      _.each(filtered, function (o) {
        servicesInitPromises.push(o.init());
      });

      Promise.all(servicesInitPromises).then(function () {
        resolve(API);
      });
    });
  });
};
