var _ = require('lodash');
var express = require('express');
var router = express.Router();
var multer  = require('multer');
var upload = multer({ dest: process.env.UPLOADS_DIR || 'uploads/' });
var _ = require('lodash');

function generator(API){
  API.routes = [];
  _.each(API.controllers, function(controller, key){
    router.use( '/' + key,
      generateRoutes('/' + key, API.models[key], controller, API)
    );
  });

  return router;
}

function generateRoutes(routerRoute, model, controller, API){
  // new Router for this model-controller
  var router = express.Router();

  if(model){
    // "extend" from base CRUD controller
    _.defaults(controller, require('./base/controller'));
    // Inject model in request
    router.all('*',function(req, res, next){
      req.model = model;
      next();
    });
  }
  var routeAuth = controller.auth || false;
  delete controller.auth;

  var routeMiddlewares = _.map((controller.middlewares || []), function (o) {
    if(_.isString(o)) o = API.middlewares[o];
    return o;
  });
  delete controller.middlewares;

  // generate CRUD, upload and custom routes
  _.each(controller, function(fn, key){
    var methods = [], route, selfAuth = routeAuth;
    if (key.charAt(0) === '$') {
      selfAuth = true;
      key = key.slice(1);
    } else if (key.charAt(0) === '_') {
      selfAuth = false;
      key = key.slice(1);
    }

    if(fn.methods) methods = fn.methods;
    if(fn.route) route = fn.route.charAt(0) === '/' ? fn.route : '/' + fn.route;

    if (fn.controller) {
      var middlewares = _.map((fn.middlewares || []), function (o) {
        if(_.isString(o)) o = API.middlewares[o];
        return o;
      });
      var controller = fn.controller;
      var auth = fn.auth;
      if (_.isBoolean(auth)) selfAuth = auth;
      fn = _.flatten([middlewares, controller]);
    } else if (!_.isArray(fn)) {
      fn = [fn];
    }

    switch (key) {
      case 'all':
        methods = ['get'];
        route = '/';
        break;
      case 'one':
        methods = ['get'];
        route = '/:id';
        break;
      case 'create':
        methods = ['post'];
        route = '/';
        break;
      case 'update':
        methods = ['put'];
        route = '/:id';
        break;
      case 'destroy':
        methods = ['delete'];
        route = '/:id';
        break;
      case 'login':
        if (API.services.auth) {
          methods = ['post'];
          route = '/login';
          fn.unshift(API.services.auth.authenticate.local);
        } else {
          console.error("Auth not enabled. Run `npm run enableauth`");
        }
        break;
      case 'upload':
        methods = ['post'];
        route = '/upload';
        fn.unshift(upload.single('file'));
        break;
      default:
        // parse custom route
        var map = key.split('__');

        if(!methods.length) {
          methods = map.shift().split('_');
        }

        if(!methods.length) {
          methods = ['get'];
        }

        _.each(methods, function (method) {
          if (['get', 'post', 'put', 'delete', 'head', 'options'].indexOf(method)===-1) {
            console.log("method not valid: ", method);
          }
        });

        if(!route) route = '/' + map.join('__');
        break;
    }

    fn = _.map(_.flatten([routeMiddlewares, fn]), function (o) {
      o = o.bind(_.assign({ API:API }, this));
      return o;
    });

    if ((routeAuth && selfAuth) || selfAuth) {
      if (API.services.auth) {
        fn.unshift(API.middlewares.isLoggedIn);
      } else {
        console.error("Auth not enabled. Run `npm run enableauth`");
      }
    }

    _.each(methods, function (method) {
      router[method](route, fn);
      API.routes.push({
        route: '/api' + routerRoute + route,
        method: method,
        auth: API.services.auth && ((routeAuth && selfAuth) || selfAuth)
      });
    });
  });

  return router;
}

module.exports = generator;
