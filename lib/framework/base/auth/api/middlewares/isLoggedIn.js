module.exports = function(req, res, next) {
  if (!req.isAuthenticated || !req.isAuthenticated() || !req.user) {
    return this.API.errors.unauthorized();
  }
  next();
};
