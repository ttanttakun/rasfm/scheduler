var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var jwt = require('express-jwt');
var lusca = require('lusca');
var requireEnv = require("require-environment-variables");

if(process.env.NODE_ENV === 'production') {
  try {
    requireEnv(['COOKIE_SECRET', 'JWT_SECRET']);
  } catch (e) {
    process.exit(1);
  }
}

module.exports.requiresApp = true;
module.exports.loadOrder = 0;

module.exports.init = function (app) {
  var API = this.API;
  API.logger.debug('Initializing auth');

  passport.use(new LocalStrategy(function (username, password, done) {
    API.models.user.find({
      where: {
        username: username
      },
      include: {
        model: API.models.passport,
        where: {
          protocol: 'local',
          status: true
        }
      }
    }).then(function (user) {
      if (!user) {
        return done(null, false);
      }

      if (!user.passports[0].validatePassword(password)) {
        return done(null, false);
      }

      return done(null, user);
    }, function (err) {
      return done(err);
    });
  }));

  passport.serializeUser(function(user, cb) {
    cb(null, user.dataValues.id);
  });

  passport.deserializeUser(function(id, cb) {
    API.models.user.find({
      where: {
        id: id
      }
    }).then(function (user) {
      return cb(null, user);
    }, function (err) {
      return cb(err);
    });
  });

  function deserializeJWT(req, res, next) {
    if (req.jwt && req.jwt.accessTokenSecret) {
      API.models.passport.find({
        where: {
          accessTokenSecret: req.jwt.accessTokenSecret
        },
        include: {
          model: API.models.user
        }
      }).then(function (passport) {
        if (passport) req.user = passport.user;
        next();
      });
    } else {
      next();
    }
  }

  app.use(require('express-session')({
    secret: process.env.COOKIE_SECRET || 'keyboard cat',
    resave: false,
    saveUninitialized: false
  }));

  app.use(passport.initialize());
  app.use(passport.session());

  app.use(jwt({
    secret: process.env.JWT_SECRET || 'secret',
    credentialsRequired: false,
    requestProperty: 'jwt'
  }));

  app.use(function(req, res, next) {
    if(req.isAuthenticated && req.isAuthenticated()) {
      if (!process.env.DISABLE_CSRF) {
        lusca.csrf()(req, res, next);
      } else {
        done();
      }
    } else {
      deserializeJWT(req, res, next);
    }
  });

  var policies = this.API.services.auth.policies;
  this.API.services.auth = {
    authenticate: {
      local: passport.authenticate('local')
    }
  };

  return new this.API.Promise(function(resolve, reject) {
    resolve();
  });
};
