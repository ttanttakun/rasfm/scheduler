before(function(done) {
  APIPromise.then(function(a){
    agent = supertest.agent(a.app);
    routes = _.filter(a.API.routes, {auth:true});
    done();
  });
});

it('all should get 401', function() {
  _.each(routes, function (route) {
    describe(route.method + ' ' + route.route, function(done) {
      it('should get 401', function(done) {
        agent[route.method](route.route)
        // .expect(200)
        .end(function (err, res) {
          expect(res.status).to.eql(401);
          done(err);
        });
      });
    });
  });
});
