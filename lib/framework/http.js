var express = require('express');
var bodyParser = require('body-parser');
var requireDir = require('../utils/requireDir');
var Promise = require('bluebird');
var routes = require('./routes');
var _ = require('lodash');
var morgan = require('morgan');

module.exports.init = function (API) {
  return new Promise(function(resolve, reject) {
    API.controllers = requireDir(__dirname + '/../../api/controllers');
    var app = express();

    app.use(morgan(API.logger.http.format, { "stream": API.logger.http.stream }));

    var errors = requireDir(__dirname + '/../../api/errors');
    app.use(function(req, res, next) {
      API.errors = {};
       _.each(errors, function(o, k){
        API.errors[k] = o.bind(_.assign({ API:API }, this), req, res, next);
      });
      next();
    });

    var middlewares = requireDir(__dirname + '/../../api/middlewares');

    API.middlewares = {};
    _.each(middlewares, function (o, k) {
      API.middlewares[k] = o.bind(_.assign({ API:API }, this));
    });

    app.use('/api',bodyParser.urlencoded({ extended: false }));
    app.use('/api', bodyParser.json());
    app.use('/', express.static(process.env.PWD + '/public'));

    if (process.env.ENABLE_CORS) {
      app.use(require('cors')());
    }

    var servicesInitPromises = [];
    var filtered = _.sortBy(
      _.filter(API.services, function(o) {
        return o.init && o.requiresApp;
      }),  function(o){
        return o.loadOrder;
      });
    _.each(filtered, function (o) {
      servicesInitPromises.push(o.init(app));
    });

    Promise.all(servicesInitPromises).then(function () {
      app.use('/api', routes(API));

      app.listen(process.env.PORT || 3000, function () {
        API.logger.info('Listening on port ' + (process.env.PORT || 3000));
        resolve({API: API, app: app});
      });
    });
  });
};
