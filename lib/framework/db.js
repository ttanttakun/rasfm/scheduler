var Sequelize = require('sequelize');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var Promise = require('bluebird');
var logger = require('./logger');
var requireDir = require('../utils/requireDir');

function DB (args) {
  var self = this;
  var MODELS_DIR = args.modelsPath || '/../../api/models';
  var sequelize = new Sequelize(
    args.dbPath || 'db.sqlite',
    args.dbUser || '',
    args.dbPassword || '',
  {
    dialect: 'sqlite',
    storage: args.dbPath,
    logging: logger.sql.debug
  });

  // Load models
  var models = requireDir(MODELS_DIR);
  _.each(models, function(model, name) {
    self[name] = sequelize.import(path.join(MODELS_DIR, name));
  });

  _.each(self, function (model) {
    if(model.associate) model.associate(self);
  });

  return new Promise(function(resolve, reject) {
    sequelize.sync().then(function() {
      logger.debug("DB sync'ed. Let's go!");
      resolve(self);
    }).catch(function(error) {
      logger.error("ERROR", error);
      reject(error);
    });
  });
}

module.exports.init = DB;
