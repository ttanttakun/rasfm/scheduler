var fs = require('fs');
var winston = require('winston');

var logsDir = process.env.LOGS_DIR || 'logs';
var morganFormat = 'common';

if (!fs.existsSync(logsDir)) {
  fs.mkdirSync(logsDir);
}

var appTransports = [
  new winston.transports.File({
    level: 'info',
    filename: logsDir + '/app.log',
    maxsize: 1048576,
    maxFiles: 10,
    colorize: false,
  })
];

var httpTransports = [
  new winston.transports.File({
    level: 'info',
    filename: logsDir + '/http.log',
    maxsize: 1048576,
    maxFiles: 10,
    colorize: false,
    json: false
  })
];

var sqlTransports = [
  new winston.transports.File({
    level: 'debug',
    filename: logsDir + '/sql.log',
    maxsize: 1048576,
    json: false,
    maxFiles: 10,
    colorize: false,
  })
];

if (process.env.ENABLE_CONSOLE || process.env.NODE_ENV === 'dev') {
  morganFormat = 'dev';
  appTransports.push(
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true
    })
  );
  httpTransports.push(
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true
    })
  );
}

if(process.env.MORGAN_FORMAT) morganFormat = process.env.MORGAN_FORMAT;

var appLogger = new winston.Logger({
  transports: appTransports
});

appLogger.http = new winston.Logger({
  transports: httpTransports
});

appLogger.http.stream = {
  write: function(message, encoding){
      appLogger.http.info(message);
  }
};
appLogger.http.format = morganFormat;

appLogger.sql = new winston.Logger({
  transports: sqlTransports
});

module.exports = appLogger;
