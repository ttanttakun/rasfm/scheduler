var spawn = require('child_process').spawn;

module.exports = function command (command, args, options, cb) {
  var job = spawn(command, args || [], options || {});
  var stdout = [];
  var stderr = [];
  job.stdout.on('data', function(data) {
    console.log('stdout: ', data.toString('utf8'));
  });
  job.stderr.on('data', function(data) {
    stderr.push(data.toString('utf8'));
    console.error('stderr: ',data.toString('utf8'));
  });
  job.on('close', function (code) {
    if (code === 0) {
      cb(null, true);
    } else {
      cb(code, null);
    }
  });
};
