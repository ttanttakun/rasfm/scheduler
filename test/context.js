var supertest = require('supertest');
var chai = require('chai');
var _ = require('lodash');

if (!process.env.DB_PATH) process.env.DB_PATH = './db.test.sqlite';

global.APIPromise = require('../index.js');
global.supertest = supertest;
global.expect = chai.expect;
global._ = _;
