describe('Controller: Example', function() {
  before(function(done) {
    APIPromise.then(function(a){
      request = supertest(a.app);
      done();
    });
  });
  describe('GET /api/example/custom-route', function() {
    it('returns something', function(done) {
      request.get('/api/example/custom-route')
        .expect(200)
        .end(function (err, res) {
          var expected = {hello: 'world'};
          expect(res.body).to.eql(expected);
          done(err);
        });
    });
  });
});
