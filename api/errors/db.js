module.exports = function (req, res, next, err) {
  return res.status(400).json({
    message: err.message,
    errors: err.errors
  });
};
