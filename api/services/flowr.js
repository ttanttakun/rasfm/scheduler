var Hose = require('flowr-hose')
var moment = require('moment')

var hose = new Hose({
  name: 'rasfm-scheduler',
  port: process.env.PORT || 0,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

module.exports = {
  createJob: function(job, params, options) {
    return hose.create(job, params, options)
  },
  init: function () {
    var API = this.API
    hose.process('aras', function (job, done) {
      var scheduleId = job.data.scheduleId
      var d = new Date()
      var year = d.getFullYear()
      var week = moment().isoWeek()
      API.services.schedule.generateARASFiles({
        scheduleId: scheduleId,
        year: year,
        week: week
      })
      .then(function (arasFiles) {
        done(null, arasFiles)
      })
      .catch(function (err) {
        done(err)
      })
    })
  }
}
