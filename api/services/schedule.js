var moment = require('moment')
var path = require('path')
const REPEAT_API = process.env.REPEAT_API || 'http://repeat.rasfm/'
const MUSIC_DIR = process.env.MUSIC_DIR || ''
const JINGLE_BLOCK = process.env.JINGLE_BLOCK || 'Random /music/jingles'
const DAY_NAMES = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
const UPDATE_COOLDOWN = process.env.UPDATE_COOLDOWN ? process.env.UPDATE_COOLDOWN * 60 * 1000 : 5 * 60 * 1000

var cooldown = {}

module.exports = {
  broadcastUpdate: function (scheduleId) {
    var API = this.API
    if (!cooldown[scheduleId]) {
      setTimeout(function () {
        var d = new Date()
        var year = d.getFullYear()
        var week = moment().isoWeek()
        API.models.schedule.findById(scheduleId)
        .then(function (schedule) {
          if (schedule.get('type') !== 'schedule') return
          API.Promise.all([
            API.services.schedule.generateARASFiles({
              scheduleId: scheduleId,
              year: year,
              week: week
            }),
            API.services.schedule.generateWeekJSON({
              scheduleId: scheduleId,
              year: year,
              week: week
            })
          ]).then(function (data) {
            var [arasFiles, scheduleJSON] = data
            API.services.flowr.createJob('aras-config:update#' + scheduleId, arasFiles)
            API.services.flowr.createJob('rasfm-json-schedule:update', {
              id: scheduleId,
              json: scheduleJSON
            })
            delete cooldown[scheduleId]
          })
        })
      }, UPDATE_COOLDOWN)
    }
  },
  generateWeekJSON: function (params) {
    var API = this.API
    return new API.Promise(function(resolve, reject) {
      var scheduleId = parseInt(params.scheduleId);
      var start = moment.utc().year(params.year).isoWeek(params.week).isoWeekday(1).hours(0).minutes(0).seconds(0).milliseconds(0).toDate()
      var end = moment.utc().year(params.year).isoWeek(params.week).isoWeekday(7).hours(23).minutes(59).seconds(0).milliseconds(999).toDate()
      API.models.event.findAll({
        where: {
          scheduleId: scheduleId,
          start: {
            $gte: start
          },
          end: {
            $lte: end
          }
        },
        include: {
          model: API.models.item,
          include: {
            model: API.models.item,
            as: 'replacement',
            include: {
              model: API.models.item,
              as: 'replacement'
            }
          }
        },
        order: [['start', 'ASC']]
      })
      .then(function(weekEvents){
        var weekJSON = {
          date: new Date().toISOString().slice(0, 10),
          id: scheduleId,
          schedule: []
        }

        weekEvents.map(function(ev) {
          ev = ev.toJSON()

          var evType
          if (ev.item.title === 'Berrikuntzak') {
            evType = 3
          } else if (ev.data.repetition) {
            evType = 2
          } else if (ev.item.type === 'irratsaioa') {
            evType = 1
          } else {
            evType = 4
          }

          var eguna = ev.start.getUTCDay() - 1
          if (eguna < 0) eguna = 6

          weekJSON.schedule.push({
            type: evType,
            izena: ev.item.title,
            eguna: eguna,
            hasiera: ev.start.toUTCString().slice(17,22),
            bukaera: ev.end.toUTCString().slice(17,22)
          })
        })

        resolve(weekJSON)
      })
    })
  },
  generateARASFiles: function (params) {
    var API = this.API
    return new API.Promise(function(resolve, reject) {
      var scheduleId = parseInt(params.scheduleId);
      var start = moment.utc().year(params.year).isoWeek(params.week).isoWeekday(1).hours(0).minutes(0).seconds(0).milliseconds(0).toDate()
      var end = moment.utc().year(params.year).isoWeek(params.week).isoWeekday(7).hours(23).minutes(59).seconds(0).milliseconds(999).toDate()
      API.Promise.all([
        API.models.event.findAll({
          where: {
            scheduleId: scheduleId,
            start: {
              $gte: start
            },
            end: {
              $lte: end
            }
          },
          include: {
            model: API.models.item,
            include: {
              model: API.models.item,
              as: 'replacement',
              include: {
                model: API.models.item,
                as: 'replacement'
              }
            }
          },
          order: [['start', 'ASC']]
        }),
        API.models.schedule.findById(scheduleId, {
          include: [{
            model: API.models.item,
            as: 'defaultItem'
          }]
        })
      ])
      .then(function(data){
        var [events, schedule] = data
        var scheduleJSON = schedule.toJSON()
        var DEFAULT_BLOCK
        if (!scheduleJSON.defaultItem) {
          DEFAULT_BLOCK = 'DEFAULT ' + JINGLE_BLOCK
        } else {
          var defaultBlockJSON = scheduleJSON.defaultItem
          switch (defaultBlockJSON.type) {
            case 'musikatartea':
              DEFAULT_BLOCK = 'DEF Random ' + path.join(MUSIC_DIR, defaultBlockJSON.data.dir) + '\n'
              DEFAULT_BLOCK += 'DEFAULT Interleave "DEF JINGLE ' + (defaultBlockJSON.data.items_per_jingle || 3) + ' 1"'
              break;
            case 'file':
              DEFAULT_BLOCK = 'DEF File ' + defaultBlockJSON.data.file_path + '\n'
              DEFAULT_BLOCK += 'DEFAULT Interleave "DEF JINGLE ' + (defaultBlockJSON.data.items_per_jingle || 3) + ' 1"'
              break;
          }
        }
        var items = {}
        var scheduleFile = ''
        events.map(function (v) {
          var ev = v.toJSON()
          var start = moment.utc(v.start)
          var day = DAY_NAMES[start.day()]
          var hour = start.format('HH:mm:ss')
          var item
          switch (ev.item.type) {
            case 'irratsaioa':
              if (ev.data.repetition) {
                item = 'L_I' + ev.item.id
              } else {
                if (ev.item.replacement) {
                  item = 'L_I' + ev.item.replacement.id
                } else {
                  item = 'DEFAULT'
                }
              }
              break
            case 'musikatartea':
              item = 'L_I' + ev.item.id
              break
            case 'file':
              item = 'L_I' + ev.item.id
              break
          }
          scheduleFile += [day, hour, item].join(' ') + '\n'
          if (!items[ev.item.id]) {
            items[ev.item.id] = ev.item
            if (ev.item.replacement) {
              if (!items[ev.item.replacement.id]) {
                items[ev.item.replacement.id] = ev.item.replacement
              }
            }
          }
        })

        var blockFile = ''
        blockFile += DEFAULT_BLOCK + '\n'
        blockFile += 'JINGLE ' + (scheduleJSON.jingleBlock || JINGLE_BLOCK) + '\n'
        API._.each(items, function (v) {
          switch (v.type) {
            case 'irratsaioa':
              blockFile += 'I' + v.id + ' File ' + REPEAT_API + v.slug + '/pop \n'
              var replacement
              if (v.replacement) {
                replacement = 'L_I' + v.replacement.id
              } else {
                replacement = 'DEFAULT'
              }
              blockFile += 'L_I' + v.id + ' Interleave "I' + v.id + ' ' + replacement +' 1 100"\n'
              break;
            case 'musikatartea':
              blockFile += 'I' + v.id + ' Random ' + path.join(MUSIC_DIR, v.data.dir) + '\n'
              blockFile += 'L_I' + v.id + ' Interleave "I' + v.id + ' JINGLE ' + (v.data.items_per_jingle || 3) + ' 1"\n'
              break;
            case 'file':
              blockFile += 'I' + v.id + ' File ' + v.data.file_path + '\n'
              blockFile += 'L_I' + v.id + ' Interleave "I' + v.id + ' JINGLE ' + (v.data.items_per_jingle || 3) + ' 1"\n'
              break;
          }
        })

        resolve({
          block: blockFile,
          schedule: scheduleFile
        })
      })
    })
  },
  generateWeek: function (params) {
    var API = this.API;
    return new API.Promise(function(resolve, reject) {
      var scheduleId = parseInt(params.scheduleId);
      API.models.schedule.findById(params.weekTemplateId, {
        include: {
          model: API.models.event
        }
      })
      .then(function(template){
        var start = moment.utc().year(params.year).isoWeek(params.week).isoWeekday(1).hours(0).minutes(0).seconds(0).milliseconds(0).toDate()
        var end = moment.utc().year(params.year).isoWeek(params.week).isoWeekday(7).hours(23).minutes(59).seconds(0).milliseconds(999).toDate()
        return API.models.event.destroy({
          where: {
            scheduleId:  scheduleId,
            start: {
              $between: [start, end]
            }
          }
        })
        .then(function(){
          var events = template.get('events');
          return API.Promise.all(events.map(function(ev){
            var start = parseDate(ev.start, params.week, params.year);
            var end = parseDate(ev.end, params.week, params.year);

            var newEvent = {
              start: start,
              end: end,
              itemId: ev.itemId,
              scheduleId: scheduleId,
              data: ev.data
            };

            return API.models.event.create(newEvent)
          }));
        })
        .then(function () {
          resolve('allright')
        });
      });
    })
  }
}

function parseDate(date, week, year){
  var d = moment.utc(date);
  var result = moment.utc()
  .isoWeek(week)
  .year(year)
  .isoWeekday(d.isoWeekday())
  .hours(d.hours())
  .minutes(d.minutes())
  .seconds(0)
  .milliseconds(0);

  return result.toDate();
}
