var moment = require('moment');
var _ = require('lodash');

module.exports = {
  all: function(req, res) {
    var query = req.query;
    var params = {};
    var whereClauses = [];

    if (query.type) {
      whereClauses.push({
        type: {
          $eq: query.type
        }
      });
    }

    if (whereClauses.length) {
      params = {
        where: {
          $and: whereClauses
        }
      };
    }

    params.include = [{
      model: this.API.models.item,
      as: 'defaultItem'
    }]

    req.model.findAll(params).then(function(items){
      return res.json(items);
    });
  },
  one: function (req, res) {
    var query = req.query;
    var params = {};
    var whereClauses = [];

    if (query.start && moment(query.start).isValid()) {
      whereClauses.push({
        start: {
          $gte: query.start
        }
      });
    }

    if (query.end && moment(query.end).isValid()) {
      whereClauses.push({
        start: {
          $lte: query.end
        }
      });
    }

    if (query.all) {
      params = {
        include: {
          model: this.API.models.event
        }
      };
    } else if (whereClauses.length) {
      params = {
        include: {
          model: this.API.models.event,
          where: {
            $and: whereClauses
          }
        }
      };
    }
    req.model.findById(req.params.id, params).then(function(items){
      return res.json(items);
    });
  },

  'post__:id/generate': function(req, res) {
    var API = this.API
    API.services.schedule.generateWeek({
      scheduleId: req.params.id,
      weekTemplateId: req.body.week_template_id,
      year: req.body.year,
      week: req.body.week
    }).then(function () {
      API.services.schedule.broadcastUpdate(req.params.id)
      res.json({
        success: true
      })
    })
  },
  'get__:id/aras': function (req, res) {
    var API = this.API
    var d = new Date()
    var year = d.getFullYear()
    var week = moment().isoWeek()
    API.services.schedule.generateARASFiles({
      scheduleId: req.params.id,
      year: year,
      week: week
    }).then(function (files) {
      res.json(files)
    })
  }
};
