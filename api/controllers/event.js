var moment = require('moment');

module.exports = {
  create: function(req, res) {
    var API = this.API
    req.model
    .create(req.body)
    .then(function(item){
      return item.setItem(req.body.item.id);
    }).then(function(item){
      return item.setSchedule(req.body.schedule.id);
    }).then(function(item) {
      API.services.schedule.broadcastUpdate(req.body.schedule.id)
      return res.json(item);
    });
  },
  update: function(req, res) {
    var API = this.API
    req.model
    .update(req.body, {where: {id: req.body.id}})
    .then(function(){
      return req.model.findById(req.body.id)
    })
    .then(function(item){
      return item.setItem(req.body.item.id)
    })
    .then(function(item) {
      API.services.schedule.broadcastUpdate(item.get('scheduleId'))
      return res.json(item);
    });
  },
  all: function(req, res) {
    var query = req.query;
    var params = {};
    var whereClauses = [];

    if (query.schedule) {
      whereClauses.push({
        scheduleId: {
          $in: [query.schedule]
        }
      });
    }

    if (query.start && moment(query.start).isValid()) {
      whereClauses.push({
        start: {
          $gte: query.start
        }
      });
    }

    if (query.end && moment(query.end).isValid()) {
      whereClauses.push({
        start: {
          $lte: query.end
        }
      });
    }

    if (whereClauses.length) {
      params = {
        where: {
          $and: whereClauses
        }
      };
    }

    if (query.items) {
      params.include = {
        model: this.API.models.item,
        include: {
          model: this.API.models.item,
          as: 'replacement',
          include: {
            model: this.API.models.item,
            as: 'replacement'
          }
        }
      };
    }

    req.model.findAll(params).then(function(items){
      return res.json(items);
    });
  }
};
