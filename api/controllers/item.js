module.exports = {
  all: function(req, res) {
    var query = req.query;
    var params = {};
    var whereClauses = [];

    if (query.type) {
      whereClauses.push({
        type: {
          $eq: query.type
        }
      });
    }

    if (query.active) {
      whereClauses.push({
        active: query.active
      });
    }

    if (whereClauses.length) {
      params = {
        where: {
          $and: whereClauses
        }
      };
    }
    params.include = {
      model: this.API.models.item,
      as: 'replacement',
      include: {
        model: this.API.models.item,
        as: 'replacement'
      }
    };

    req.model.findAll(params).then(function(items){
      return res.json(items);
    });
  },
  create: function(req, res) {
    req.model
    .create(req.body)
    .then(function(item){
      if (req.body.replacement) {
        item.setReplacement(req.body.replacement.id)
        .then(res.json(item));
      } else {
        return res.json(item);
      }
    });
  },
  update: function(req, res) {
    var API = this.API;
    req.model.findById(req.body.id)
    .then(function(item){
      return item.update(req.body);
    }).then(function(item){
      if (req.body.replacement) {
        item.setReplacement(null).then(function(){
          return item.setReplacement(req.body.replacement.id);
        }).then(function (item) {
          res.json(item);
        });
      } else {
        return res.json(item);
      }
    });
  },
};
