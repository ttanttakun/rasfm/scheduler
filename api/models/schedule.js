var _ = require('lodash');

module.exports = function(sequelize, DataTypes) {
  var Schedule = sequelize.define('schedule', {
    title: DataTypes.STRING,
    slug: DataTypes.STRING,
    description: DataTypes.TEXT,
    jingleBlock: DataTypes.TEXT,
    type: {
      type:   DataTypes.ENUM,
      values: ['template', 'schedule']
    },
    data: {
      type: DataTypes.TEXT,
      get: function () {
        var val = this.getDataValue('data');
        if (_.isString(val)) {
          val = JSON.parse(val);
        }
        return val;
      },
      set: function (val) {
        if (_.isObject(val)) {
          val = JSON.stringify(val);
        }
        this.setDataValue('data', val);
      }
    }
  }, {
    classMethods: {
      associate: function (models) {
        Schedule.hasMany(models.event);
        Schedule.belongsTo(models.item, {as: 'defaultItem', foreignKey: 'defaultItemId'})
      }
    }
  });

  return Schedule;
};
