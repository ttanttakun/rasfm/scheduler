var _ = require('lodash');

module.exports = function(sequelize, DataTypes) {
  var Event = sequelize.define('event', {
    start: DataTypes.DATE,
    end: DataTypes.DATE,
    data: {
      type: DataTypes.TEXT,
      allowNull: true,
      get: function () {
        var val = this.getDataValue('data');
        if (_.isString(val)) {
          val = JSON.parse(val);
        }
        return val ? val : {};
      },
      set: function (val) {
        if (_.isObject(val)) {
          val = JSON.stringify(val);
        }
        this.setDataValue('data', val);
      }
    }
  }, {
    classMethods: {
      associate: function (models) {
        Event.belongsTo(models.item);
        Event.belongsTo(models.schedule);
      }
    }
  });

  return Event;
};
