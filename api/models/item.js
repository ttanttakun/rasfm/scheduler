var _ = require('lodash');

module.exports = function(sequelize, DataTypes) {
  var Item =  sequelize.define('item', {
    title: DataTypes.STRING,
    slug: DataTypes.STRING,
    description: DataTypes.TEXT,
    type: DataTypes.STRING,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    data: {
      type: DataTypes.TEXT,
      allowNull: true,
      get: function () {
        var val = this.getDataValue('data');
        if (_.isString(val)) {
          val = JSON.parse(val);
        }
        return val ? val : {};
      },
      set: function (val) {
        if (_.isObject(val)) {
          val = JSON.stringify(val);
        }
        this.setDataValue('data', val);
      }
    }
  }, {
    classMethods: {
      associate: function (models) {
        Item.hasMany(models.event);
        Item.hasOne(Item, {as: 'replacement'});
      }
    }
  });

  return Item;
};
